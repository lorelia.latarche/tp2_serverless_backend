# resource aws_s3_bucket s3_job_offer_bucket

resource "aws_s3_bucket" "s3_job_offer_bucket" {
  bucket = "lorelia-bucket-tp2"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}


# resource aws_s3_object job_offers
resource "aws_s3_object" "job_offers" {
  bucket = aws_s3_bucket.s3_job_offer_bucket.bucket
  key    = "job_offers/"
}


# resource aws_s3_bucket_notification bucket_notification
resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.s3_job_offer_bucket.bucket

  lambda_function {
    lambda_function_arn = aws_lambda_function.s3_to_sqs_lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = aws_s3_object.job_offers.key
    filter_suffix       = ".csv"
  }
}

